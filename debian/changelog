cyvcf2 (0.30.11-1) unstable; urgency=medium

  * Fix watch URL
  * New upstream version 0.30.11
  * Remove merged patches amd Refresh
    d/p/i386.patch
  * d/tests: Also run build time tests as autopkgtests

 -- Nilesh Patra <nilesh@debian.org>  Sun, 15 Aug 2021 17:05:52 +0530

cyvcf2 (0.30.4-4) unstable; urgency=medium

  * Disable failing test on i386 using
    dpkg-architecture

 -- Nilesh Patra <npatra974@gmail.com>  Wed, 03 Feb 2021 17:57:14 +0530

cyvcf2 (0.30.4-3) unstable; urgency=medium

  * Attempt fixing i386 FTBFS with skipping
    a test that ends up with NaN values

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 26 Jan 2021 23:36:46 +0530

cyvcf2 (0.30.4-2) unstable; urgency=medium

  * Pull Upstream PR to fix ppc64el build
    (Closes:#956675)

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 26 Jan 2021 16:12:46 +0530

cyvcf2 (0.30.4-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Sun, 17 Jan 2021 09:49:20 +0100

cyvcf2 (0.30.2-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 24 Dec 2020 15:23:21 +0530

cyvcf2 (0.30.1-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Sun, 06 Dec 2020 20:18:35 +0530

cyvcf2 (0.30.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.1 (routine-update)

 -- Nilesh Patra <npatra974@gmail.com>  Fri, 04 Dec 2020 19:58:00 +0530

cyvcf2 (0.20.9-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 12 Oct 2020 11:07:26 +0530

cyvcf2 (0.20.8-1) unstable; urgency=medium

  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Sat, 03 Oct 2020 20:08:24 +0530

cyvcf2 (0.20.7-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Nilesh Patra <npatra974@gmail.com>  Mon, 28 Sep 2020 22:45:14 +0530

cyvcf2 (0.20.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 18 Sep 2020 08:19:58 +0200

cyvcf2 (0.20.1-1) unstable; urgency=medium

  * Team Upload.
  * New upstream version 0.20.1 (Closes: #964677)
  * Refresh patch and cythonize modules
  * Do not import numpy during clean
  * compat version: 13

 -- Nilesh Patra <npatra974@gmail.com>  Thu, 09 Jul 2020 23:17:28 +0530

cyvcf2 (0.11.6-2) unstable; urgency=medium

  * Team upload.
  * Bug #954640 of python3-humanfriendly was fixed
    Closes: #954603
  * Add salsa-ci file (routine-update)
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Mon, 30 Mar 2020 10:00:07 +0200

cyvcf2 (0.11.6-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.5.0 (routine-update)
  * Adjusted spelling fixes - 4 already fixed upstream,
    submitted PR for the last on
    https://github.com/brentp/cyvcf2/pull/140

 -- Steffen Moeller <moeller@debian.org>  Tue, 25 Feb 2020 14:57:29 +0100

cyvcf2 (0.11.5-2) unstable; urgency=medium

  * Team upload.
  * debian/patches/spelling: fix a few typos.
  * Rules-Requires-Root: no
  * Add a man page for cyvcf2

 -- Michael R. Crusoe <michael.crusoe@gmail.com>  Tue, 17 Dec 2019 15:47:52 +0100

cyvcf2 (0.11.5-1) unstable; urgency=medium

  * Team upload.
  * Now run full test suite since data file is provided

 -- Andreas Tille <tille@debian.org>  Thu, 01 Aug 2019 16:07:46 +0200

cyvcf2 (0.11.4-1) unstable; urgency=medium

  * Team upload.
  * Drop Python2 package since it has no rdepends
  * Delete debian/README.source since outdated
  * debhelper-compat 12
  * Standards-Version: 4.4.0
  * Exclude test that needs missing data file
  * Cleanup after testing

 -- Andreas Tille <tille@debian.org>  Thu, 01 Aug 2019 13:03:46 +0200

cyvcf2 (0.10.8-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 12 Feb 2019 22:12:40 +0100

cyvcf2 (0.10.4-1) unstable; urgency=medium

  [ Andreas Tille ]
  * Team upload.
  * New upstream version (should build using Debian packaged htslib)
  * Standards-Version: 4.3.0
  * Simplify watch file
  * debhelper 12
  * Remove trailing whitespace in debian/copyright
  * Build-Depends: libhts-dev, libbz2-dev
  * Binaries are Section: python
  * Do not mention htslib in copyright since it is not shipped with the
    source tarball

  [ Liubov Chuprikova ]
  * A patch that adds link dependency on htslib

 -- Andreas Tille <tille@debian.org>  Sat, 12 Jan 2019 00:16:33 +0100

cyvcf2 (0.10.0+git20180727-2) unstable; urgency=medium

  * Build-Depends: add some missing dependencies
  * Set default locale to C.UTF-8

  [ Steffen Moeller ]
  * Added Liubov Chuprikova to uploaders

  [ Andreas Tille ]
  * Standards-Version: 4.2.0
  * cme fix dpkg-control
  * hardening=+all

 -- Liubov Chuprikova <chuprikovalv@gmail.com>  Tue, 14 Aug 2018 12:49:59 +0200

cyvcf2 (0.10.0+git20180727-1) unstable; urgency=medium

  * Not a new upstream release, but feels like it.
    - Falling back on modified htslib redistributed with cyvcf2 source.
      Please everyone have an eye on how this develops and make some
      noise on our list and in README.source.
    - Removed libhts* from build dependencies.
  * No more patches required with many thanks to
    - Brent Pedersen (upstream)
    - Liubov Chuprikova
  * Enabled testing.
  * Initial release (Closes: #890233)

 -- Steffen Moeller <moeller@debian.org>  Sat, 28 Jul 2018 01:13:58 +0200

cyvcf2 (0.10.0-1) UNRELEASED; urgency=medium

  * New upstream release.

 -- Steffen Moeller <moeller@debian.org>  Sat, 28 Jul 2018 00:12:36 +0200

cyvcf2 (0.9.0-1) UNRELEASED; urgency=medium

  * New upstream version.

 -- Steffen Moeller <moeller@debian.org>  Sat, 07 Jul 2018 22:25:23 +0200


cyvcf2 (0.8.6-1) UNRELEASED; urgency=medium

  * Initial packaging.

 -- Steffen Moeller <moeller@debian.org>  Sun, 11 Feb 2018 19:16:31 +0100
